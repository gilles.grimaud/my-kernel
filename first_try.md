% My First First Software
% Gilles Grimaud 
% January 2025

---

# My First First Software

You have been writing software for several years now, and yet, many of you have never written a "first software," that is, software that runs first when the machine starts. That is what we will do now. 

You can find a minimal software example in the following link to the [repository `my-kernel`](https://gitlab.univ-lille.fr/gilles.grimaud/my-kernel).
The provided Makefile compiles the program located in `src/main.c` and produces the ISO image of a bootable disk for x86 architecture. 
To run this "first software," you will need to either:

1. Flash the ISO image onto a persistent medium (USB key, disk); or 
2. Use a virtual machine to boot from the produced ISO image. 

We recommend this second solution during the development and testing phase as it is more convenient than the first (compilation/execution cycles are shorter and debugging is easier).

To start an ISO image from the command line, we recommend using 
[qemu](https://www.qemu.org). 

```sh
qemu-system-x86_64 -boot d -m 2048 -cdrom mykernel.iso -curses
```

This command is directly available in the provided `Makefile` with: 

```sh
make run
``` 

To produce an ISO image from an executable binary, we use the grub utility.
You can find the build process we propose in the `Makefile`. To use it, simply run:   
```bash
make
```

The Makefile first compiles your source file into a `.o` file, then links your `.o` with other elements to produce a binary file. Finally, it uses the utilities grub-mkrescue
and xorriso to create the ISO file. 
Note that in this process, technically, the first first program executed
is the BIOS, which loads the grub software. Grub then displays a menu that allows loading and 
running your software (as if it were loading an operating system kernel). 

To ensure everything works properly, you will need the following tools:

- `gcc`
- `grub-common`
- `xorriso`
- `qemu`

These tools are installed on the machines in the lab rooms. You can connect to them (via VPN) with:
```sh
ssh <login>@a<#room>p<#station>.fil.univ-lille1.fr  
```

Otherwise, you can install the tools on your preferred Linux distribution. Installation on
Microsoft Windows and macOS X is not impossible, but it is more complicated... Some explanations
are given in the README.md of the repository to set up macOS X.  

As previously explained, it is a `.c` file that is compiled to produce 
the ISO image. However, it is important to note that the C language used here is "bare-metal" C. 
Unlike a standard C program, it does not run "on top" of an operating system,
and you do not have access to any of the libraries you are used to. Thus, functions such 
as `printf`, `malloc`, `fopen`, `fork`, `exit`... which are implemented by `glibc` and use operating system services are not available.

To assist you, the provided `main.c` file implements two basic functions.
The first initializes the fundamental mechanisms of Intel architectures. This includes the
[GDT](https://en.wikipedia.org/wiki/Global_Descriptor_Table). The initialization provided
by `main.c` grants access to all the machine's memory without restriction. 
It also initializes the [IDT](https://en.wikipedia.org/wiki/Interrupt_descriptor_table), which manages
interrupt mechanisms within the microprocessor on Intel architectures. 

The second function provided by `main.c` is a minimalist implementation
of `putc()`, `puts()`, and `puthex()`. These functions allow you to send characters to the screen 
and display information. The screen is configured to operate in "text" mode with 80 columns and 25 rows. In this video mode (defined by IBM VGA standards), 
the graphics controller (the graphics card) shares part of its memory with the 
processor. From the processor's perspective, the graphics controller's memory is accessible at
address `0xA0000`. However, in text mode, the data used by the graphics card to 
produce the display are accessible to the processor starting at address `0xb8000`. Additionally, the 
graphics controller can manage cursor blinking using specific hardware registers. The `putc()` function implementation programs this register to handle the hardware cursor. To program hardware device registers, Intel architectures 
provide the `in` and `out` machine instructions. The provided codebase defines 
in `include/ioport.h` a C function `unsigned char _inb(int port);` and another function 
`void _outb(int port, unsigned char val);`, which are specifically used to control the cursor position.     

---

# And how do we test it?

First, we will examine the operation of a typical keyboard controller
on Intel architectures, the PS2 controller. We chose not to use the USB keyboard controller in this 
exercise because handling USB communications would unnecessarily complicate the exercise. 

You can find a description of the keyboard controller's operation 
on [osdev.org](https://wiki.osdev.org/%228042%22_PS/2_Controller). It explains
that the keyboard is accessible through two registers associated with ports `0x60` and `0x64`. It states that 
the first is called _data port_ and can be read or written, while the second is called
_status register_ when read, and _command register_ when written. 

Furthermore, the keyboard processor generates an interrupt of level 1 when a key is
pressed.    

---

## Question 1: My first first hello world.

Create a program that simply displays "hello world" when your ISO image
boots. To display Hello World, you can use the `puts()` function provided 
in the code available in the repository. 

## Question 2: My first first access to a hardware device. 

Create a program that displays the keyboard code returned by the keyboard
controller when a key is pressed. To do this, simply create an infinite loop that reads
the keyboard code produced on port 0x60 using `_inb()` and writes the number read to the screen
using, for example, `puthex()` and `putc()`.  


