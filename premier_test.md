% Mon premier premier logiciel
% Gilles Grimaud
% Janvier 2025

---

# Mon premier premier logiciel

Cela fait maintenant quelques années que vous écrivez des logiciels, et pourtant, nombreux parmi
vous sont ceux qui n'ont encore jamais écrit un "premier logiciel", c'est-à-dire un logiciel qui
s'exécute en premier, lorsque la machine démarre. C'est ce que nous allons faire maintenant.

Vous trouverez en suivant ce lien vers le [dépôt `my-kernel`](https://gitlab.univ-lille.fr/gilles.grimaud/my-kernel)
qui vous propose un logiciel minimal. Le Makefile proposé compile le programme
présent dans `src/main.c` et produit l'image ISO d'un disque amorçable sur architecture x86.
Pour exécuter ce "premier logiciel", il vous faudra soit :

1. Flasher l'image ISO sur un support persistant (clé USB, disque) ; soit
2. Utiliser une machine virtuelle pour démarrer sur l'image ISO produite.

Nous privilégions cette seconde solution dans une phase de développement et de test, car elle est
plus confortable que la première (les cycles compilation/exécution sont plus courts et le débogage
en est facilité).

Pour démarrer une image ISO en ligne de commande, nous vous recommandons l'utilisation de
[qemu](https://www.qemu.org).

```sh
qemu-system-x86_64 -boot d -m 2048 -cdrom mykernel.iso -curses
```

Cette commande est directement disponible dans le `Makefile` proposé avec :

```sh
make run
```

Pour produire une image ISO à partir d'un binaire exécutable, nous utilisons l'utilitaire grub.
Vous pouvez voir la recette de cuisine que nous vous proposons dans le `Makefile`. Pour
l'utiliser, faites simplement :   
```bash
make
```

Le Makefile compile d'abord votre fichier source en un `.o`, puis il lie votre `.o` avec quelques
autres éléments et produit un fichier binaire. Enfin, il utilise les utilitaires grub-mkrescue
et xorriso pour produire le fichier ISO.
Notez qu'en procédant de la sorte, techniquement, le premier premier programme exécuté
est le BIOS, qui charge le logiciel grub, puis grub affiche un menu qui permet de charger et de
lancer votre logiciel (comme s'il lançait un noyau de système d'exploitation).

Notez donc que pour que cela fonctionne correctement vous aurez besoin des outils suivants :

- `gcc`
- `grub-common`
- `xorriso`
- `qemu`

Ces outils sont installés sur les machines des salles de TP. Vous pouvez vous y connecter (via le
VPN) avec :
```sh
ssh <login>@a<#salle>p<#poste>.fil.univ-lille1.fr  
```

Sinon, vous pouvez installer les outils sur votre distribution Linux préférée. L'installation sur
Microsoft Windows et macOS X n'est pas impossible, mais elle est plus délicate... Quelques explications
sont données dans le README.md du dépôt pour mettre en place un setup macOS X.  

Comme cela a été expliqué précédemment, c'est un fichier `.c` qui est compilé pour produire
l'image ISO. Cependant, il est important de noter que le langage C utilisé ici est du C "bare-metal".
Contrairement à un programme C classique, il ne s'exécute pas "au-dessus" d'un système d'exploitation
et vous ne disposez donc d'aucune des bibliothèques dont vous avez l'habitude. Ainsi, des fonctions telles
que `printf`, `malloc`, `fopen`, `fork`, `exit`... qui sont implémentées par la `glibc` et qui
utilisent des services de votre système d'exploitation ne sont pas disponibles.

Pour vous aider, le fichier `main.c` que l'on vous propose réalise néanmoins deux fonctions de base.
La première est d'initialiser les mécanismes fondamentaux des architectures Intel. Il s'agit d'une
part de la [GDT](https://en.wikipedia.org/wiki/Global_Descriptor_Table). L'initialisation mise en
place par le `main.c` fourni vous donne accès à toute la mémoire de la machine, sans restriction.
D'autre part, il s'agit de l'[IDT](https://en.wikipedia.org/wiki/Interrupt_descriptor_table) qui gère
sur les architectures Intel les mécanismes d'interruption au sein du microprocesseur.

La seconde fonction qu'assure le fichier `main.c` est de vous proposer une implémentation minimaliste
des fonctions `putc()`, `puts()` et `puthex()`. Grâce à ces fonctions, vous pouvez envoyer sur l'écran
des caractères, et donc afficher des informations. L'écran est configuré pour fonctionner en mode
"texte" 80 colonnes, 25 lignes. Dans ce mode vidéo (défini dans les normes VGA par IBM),
le contrôleur graphique (la carte graphique) partage un segment de sa mémoire avec le
microprocesseur. Du point de vue du processeur, la mémoire du contrôleur graphique est accessible à
l'adresse `0xA0000`. Mais en mode texte, les informations utilisées par la carte graphique pour
produire l'image sont accessibles au microprocesseur à partir de l'adresse `0xb8000`. De plus, le
contrôleur graphique peut gérer le clignotement d'un curseur via des registres matériels
spécifiques. L'implémentation des fonctions `putc()` programme ce registre pour gérer le curseur
matériel. Pour programmer les registres des périphériques matériels, les architectures Intel
proposent les instructions machines `in` et `out`. La base de code que l'on vous propose définit
dans `include/ioport.h` une fonction C `unsigned char _inb(int port);` et une fonction C
`void _outb(int port, unsigned char val);`, qui sont notamment utilisées pour piloter la position
du curseur matériel.     

---

# Et pour tester tout ça ?

Dans un premier temps, nous allons nous intéresser au fonctionnement d'un contrôleur de clavier
typique des architectures Intel, le contrôleur PS2. Nous n'avons pas choisi d'utiliser dans ce
sujet le contrôleur de clavier USB car la gestion des communications USB complique inutilement
l'exercice proposé.

Vous pourrez trouver la description du fonctionnement du contrôleur clavier de vos machines
sur le site [osdev.org](https://wiki.osdev.org/%228042%22_PS/2_Controller). On peut notamment y découvrir
que le clavier est accessible via deux registres associés aux ports `0x60` et `0x64`. On y lit que
le premier est appelé _data port_, et qu'il peut être lu ou écrit, alors que le second est appelé
_status register_ quand on le lit, et _command register_ quand on l'écrit.

Par ailleurs, le processeur clavier génère une interruption de niveau 1 quand une touche est
pressée.    

---

## Question 1 : Mon premier premier hello world.

Réaliser un premier programme qui affiche simplement "hello world" lorsque votre image ISO
démarre. Pour afficher Hello World, vous pourrez utiliser la fonction `puts()` proposée
dans le code fourni avec le dépôt.

## Question 2 : Mon premier premier accès à un périphérique matériel.

Réalisez un premier programme qui affiche simplement le code clavier retourné par le contrôleur
clavier lorsqu'on tape une touche. Pour cela, réalisez simplement une boucle infinie qui lit
le code clavier produit sur le port 0x60 avec des `_inb()` et écrit le nombre lu sur l'écran
en utilisant par exemple `puthex()` et `putc()`.  


